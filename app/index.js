import { TRACKS } from './tracks.js';
import Track from './track.js';
import PlayingBars from './playing-bars.js';
import { formatTime, calculateWidthFromStep } from './utils/timer.js';

class App {

    constructor() {
        // DOM Elements
        this.elPlaylist = document.getElementById('playlist-tracks');
        this.elPlayBtn = document.getElementById('btn-control-play');
        this.elPauseBtn = document.getElementById('btn-control-pause');
        this.elPrevBtn = document.getElementById('btn-control-prev');
        this.elNextBtn = document.getElementById('btn-control-next');
        this.elProgress = document.getElementById('track-progress');
        this.elCurrentTrackTitle = document.getElementById('current-track_title');
        this.elCurrentTrackArtist = document.getElementById('current-track_artist');
        this.elCurrentTrackTimer = document.getElementById('current-track_timer');

        // Properties
        this.currentTrackIndex = 0;
        this.playingBars = new PlayingBars();
        this.tracks = TRACKS.map(track => new Track(track));

        // Chain methods
        return this;
    }

    init() {
        this.bindEvents();
        this.render();
    }

    bindEvents() {
        this.elPlayBtn.addEventListener('click', this.playTrack.bind(this));
        this.elPauseBtn.addEventListener('click', this.pauseTrack.bind(this));
        this.elNextBtn.addEventListener('click', this.nextTrack.bind(this));
        this.elPrevBtn.addEventListener('click', this.prevTrack.bind(this));
    }

    setActiveTrack() {
        Array.from(this.elPlaylist.children).forEach((elTrack, index) => {
            elTrack.className = elTrack.className.replace(/is-active/gi, '');
            const bars = elTrack.querySelectorAll('.bars');

            if (bars.length > 0) {
                elTrack.removeChild(bars[0]);
            }
            if (this.currentTrackIndex === index) {
                elTrack.className += ' is-active';
                elTrack.prepend(this.playingBars.create());
            }
        });
    }

    render() {
        this.elPlaylist.innerHTML = '';
        this.tracks.forEach((track, index) => {
            const elTrack = track.render(() => {
                this.currentTrackIndex = index;
                this.playTrack();
            });

            this.elPlaylist.appendChild(elTrack);
        });
    }

    // DOM Events
    playTrack() {
        // Stop all music.
        if (this.tracks[this.currentTrackIndex].isPaused === false) {
            Howler.stop();
        }
       
        this.elCurrentTrackTitle.innerText = 'Loading track...';
        this.tracks[this.currentTrackIndex].create(this.onTrackPlay.bind(this)).play();
    }

    pauseTrack() {
        this.tracks[this.currentTrackIndex].pause();
        this.elPauseBtn.parentElement.style.display = 'none';
        this.elPlayBtn.parentElement.style.display = 'block';
        const bars = this.elPlaylist.querySelectorAll('.bars');
        if (bars.length > 0) {
            bars[0].className += ' paused';
        }
    }

    nextTrack() {
        this.currentTrackIndex++;
        if (this.currentTrackIndex > this.tracks.length - 1) {
            this.currentTrackIndex = 0;
        }
        this.playTrack();
    }

    prevTrack() {
        this.currentTrackIndex--;
        if (this.currentTrackIndex < 0) {
            this.currentTrackIndex = this.tracks.length - 1;
        }
        this.playTrack();
    }

    // Howler Events.
    onTrackPlay() {
        // Track is now playing!
        this.setActiveTrack();
        this.elCurrentTrackTitle.innerText = this.tracks[this.currentTrackIndex].title;
        this.elCurrentTrackArtist.innerText = this.tracks[this.currentTrackIndex].artist;
        this.elPlayBtn.parentElement.style.display = 'none';
        this.elPauseBtn.parentElement.style.display = 'block';
        this.step();
    }

    step() {
        // Taken from Howler Audio Example code.
        // Get the Howl we want to manipulate.
        const sound = this.tracks[this.currentTrackIndex].howl;

        // Determine our current seek position.
        const seek = sound.seek() || 0;
        this.elCurrentTrackTimer.innerText = formatTime(Math.round(seek)) + ' - ' + formatTime( Math.round(sound.duration() ));
        this.elProgress.style.width = calculateWidthFromStep( seek, sound );

        // If the sound is still playing, continue stepping.
        if (sound.playing()) {
            requestAnimationFrame(this.step.bind(this));
        }
    }
}

new App().init();