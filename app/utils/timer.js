export const formatTime = (secs) =>{
    const minutes = Math.floor(secs / 60) || 0;
    const seconds = (secs - minutes * 60) || 0;
    return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
}

export const calculateWidthFromStep = (seek, sound) => {
    return (((seek / sound.duration()) * 100) || 0) + '%';
}