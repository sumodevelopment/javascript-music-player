class PlayingBars {
    constructor(barCount = 5) {
        this.barCount = barCount;
    }

    create() {
        const elPlayingBars = document.createElement('div');
        elPlayingBars.className = 'bars';
        for ( let i = 0; i <  this.barCount; i++){
            const elBar = document.createElement('div');
            elBar.className = 'bar';
            elPlayingBars.appendChild( elBar );
        }
        return elPlayingBars;
    }
}

export default PlayingBars;