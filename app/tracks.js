// DUMMY DATA 
// Id's are not being used.
export const TRACKS = [
    {
        id: 1,
        title: 'Tech House vibes',
        artist: 'Alejandro Magaña (A. M.)',
        src: 'tracks/mixkit-tech-house-vibes-130.mp3',
        howl: null
    },
    {
        id: 2,
        title: 'Hazy After Hours',
        artist: 'Alejandro Magaña (A. M.)',
        src: 'tracks/mixkit-hazy-after-hours-132.mp3',
        howl: null
    },
    {
        id: 3,
        title: 'Hip Hop 02',
        artist: 'Lily J',
        src: 'tracks/mixkit-hip-hop-02-738.mp3',
        howl: null
    },
    {
        id: 4,
        title: 'Just Kidding',
        artist: 'Ahjay Stelino',
        src: 'tracks/mixkit-just-kidding-11.mp3',
        howl: null
    },

    // DUPLICATES
    {
        id: 1,
        title: 'Tech House vibes',
        artist: 'Alejandro Magaña (A. M.)',
        src: 'tracks/mixkit-tech-house-vibes-130.mp3',
        howl: null
    },
    {
        id: 2,
        title: 'Hazy After Hours',
        artist: 'Alejandro Magaña (A. M.)',
        src: 'tracks/mixkit-hazy-after-hours-132.mp3',
        howl: null
    },
    {
        id: 3,
        title: 'Hip Hop 02',
        artist: 'Lily J',
        src: 'tracks/mixkit-hip-hop-02-738.mp3',
        howl: null
    },
    {
        id: 4,
        title: 'Just Kidding',
        artist: 'Ahjay Stelino',
        src: 'tracks/mixkit-just-kidding-11.mp3',
        howl: null
    },
    {
        id: 1,
        title: 'Tech House vibes',
        artist: 'Alejandro Magaña (A. M.)',
        src: 'tracks/mixkit-tech-house-vibes-130.mp3',
        howl: null
    },
    {
        id: 2,
        title: 'Hazy After Hours',
        artist: 'Alejandro Magaña (A. M.)',
        src: 'tracks/mixkit-hazy-after-hours-132.mp3',
        howl: null
    },
    {
        id: 3,
        title: 'Hip Hop 02',
        artist: 'Lily J',
        src: 'tracks/mixkit-hip-hop-02-738.mp3',
        howl: null
    },
    {
        id: 4,
        title: 'Just Kidding',
        artist: 'Ahjay Stelino',
        src: 'tracks/mixkit-just-kidding-11.mp3',
        howl: null
    }
]