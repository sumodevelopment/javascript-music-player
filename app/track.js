class Track {
    constructor({ title, artist, src }) {
        this.title = title;
        this.artist = artist;
        this.src = src;
        this.howl = null;
        this.isPaused = false;
    }

    create(onplay) {
        if (this.howl === null) {
            this.howl = new Howl({
                src: [this.src],
                autoplay: false,
                onplay: onplay,
                onpause: this.onTrackPaused.bind(this)
            });
        }
        return this;
    }

    play() {
        this.isPaused = false;
        this.howl.play();
    }

    stop() {
        this.howl.stop();
    }

    pause() {
        this.howl.pause();
    }

    onTrackPaused() {
        this.isPaused = true;
    }

    render(onTrackPlayClicked) {
        const elTrack = document.createElement('div');
        elTrack.className = 'panel-block';

        const elTrackInfo = document.createElement('div');
        
        const elTrackTitle = document.createElement('h4');
        elTrackTitle.innerText = this.title;
        elTrackInfo.appendChild(elTrackTitle);

        const elTrackArtist = document.createElement('small');
        elTrackArtist.innerText = this.artist;
        elTrackInfo.appendChild( elTrackArtist );

        elTrack.appendChild( elTrackInfo );

        elTrack.addEventListener('click', onTrackPlayClicked);

        return elTrack;
    }


}

export default Track;