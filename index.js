const express = require('express');
const app = express();
const{ PORT = 3000 } = process.env;

app.use( express.static('app') );

app.get('/', (req, res) => {
    return res.sendFile( __dirname + '/app/index.html' );
});

app.listen(PORT, ()=> console.log(`Server running on port ${ PORT }`));